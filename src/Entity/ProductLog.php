<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductLogRepository")
 */
class ProductLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="integer")
     */
    private $productId;

    public function setProductId($productId)
    {
        $this->productId = $productId;
    }

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $oldPrice;

    public function setOldPrice($oldPrice)
    {
        $this->oldPrice = $oldPrice;
    }

    /**
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     */
    private $newPrice;

    public function setPrice($newPrice)
    {
        $this->newPrice = $newPrice;
    }

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    private $createdAt;

    public function setDate()
    {
        $this->createdAt = new \DateTime(date('Y-m-d H:i:s'));
    }

    /**
     * @ORM\Column(type="string", name="action", length=50)
     */
    private $action;

    public function setAction($action)
    {
        $this->action = $action;
    }
}
