$(document)
    .on('click', '[data-update-id]', function () {
        var $this = $(this);
        var id = $this.data('update-id');
        var newPrice = $('[data-price-product-id="' + id + '"]').val();

        $.ajax({
            url: $this.attr('href'),
            method: 'POST',
            data: {price: newPrice},
            success: function (data) {
                console.log(data);
            }
        });
        return false
    })
    .on('click', '[data-delete-id]', function () {
        var $this = $(this);
        var id = $this.data('delete-id');

        $.ajax({
            url: $this.attr('href'),
            method: 'POST',
            success: function (data) {
                $('#product-' + id).remove();
                console.log(data);
            }
        });
        return false
    });