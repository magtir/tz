<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="string", length=100)
     * @Assert\NotBlank(
     *     message="Это поле обязательное для заполнения."
     * )
     */
    private $name;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @ORM\Column(type="decimal", scale=2)
     * @Assert\NotBlank(
     *     message="Это поле обязательное для заполнения."
     * )
     * @Assert\Type(
     *     type="integer",
     *     message="Ваше значение '{{ value }}' не является типом '{{ type }}'."
     *    )
     */
    private $price;

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }
}
