<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\ProductLog;
use App\Form\CreateProductForm;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends Controller
{

    /**
     * @Route("/product/create", name="create")
     */
    public function create(Request $request)
    {
        $product = new Product();

        $form = $this->createForm(CreateProductForm::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);

            $em->flush();

            $this->logging("create", $product);

            return $this->redirectToRoute('list');
        }

        return $this->render('pages/create/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('pages/home/home.html.twig');
    }

    /**
     * @Route("/product/list", name="list")
     */
    public function list()
    {
        $products = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findAll();

        return $this->render('pages/product/list.html.twig', ['products' => $products]);
    }

    /**
     * @Route("/product/update/{id}", name="update")
     */
    public function update($id)
    {
        if (isset($_POST['price']) && $_POST['price']) {
            $em = $this->getDoctrine()->getManager();
            $product = $em->getRepository(Product::class)->find($id);

            $newPrice = $_POST['price'];

            if (!$product) {
                return $this->json("Продукт с id: " . $id . " не найден");
            }

            $this->logging("update", $product, $newPrice);

            $product->setPrice($newPrice);
            $em->flush();

            return $this->json("Цена у продукта id: " . $id . " изменена");
        }

        return $this->json("Ни чего не изменено");
    }

    /**
     * @Route("/product/delete/{id}", name="delete")
     */
    public function delete($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository(Product::class)->find($id);

        if (!$product) {
            return $this->json("Продукт с id: " . $id . " не найден");
        }

        $this->logging("delete", $product);

        $em->remove($product);
        $em->flush();

        return $this->json("Продукт с id: " . $id . " удален");
    }


    public function logging($action, $model, $newPrice = null)
    {
        $emLog = $this->getDoctrine()->getManager();
        $productLog = new ProductLog();

        $productLog->setProductId($model->getId());
        $productLog->setName($model->getName());
        $productLog->setOldPrice($model->getPrice());

        if ($action == "update") {
            $productLog->setPrice($newPrice);
        }

        $productLog->setAction($action);
        $productLog->setDate();
        $productLog->setAction($action);

        $emLog->persist($productLog);
        $emLog->flush();
    }
}

?>